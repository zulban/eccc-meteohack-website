import os

from flask import Flask
from app.views import views_blueprint

def is_reloader_process():
    "werkzeug launches the Flask app twice - the second time to track code changes for reloading."
    "This returns true when this is the second launched process."
    return bool(os.environ.get('WERKZEUG_RUN_MAIN'))

def create_app(env=""):
    try:
        app = Flask(__name__)
        app.register_blueprint(views_blueprint)
        return app
    except:
        app.logger.exception("Failed to build app on start up.")
        raise

app=create_app()