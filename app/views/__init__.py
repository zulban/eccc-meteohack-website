
from flask import Blueprint
views_blueprint = Blueprint(
    "views_blueprint",
     __name__,
     static_folder="static")

from app.views import public
