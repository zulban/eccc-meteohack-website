"""
The contents of this folder are generally useful and can be copied, without modification, to other python projects.
"""

from constants import *

from utilities.colors import *
from utilities.docopt import *
from utilities.path import *
from utilities.language import *